<?php
/**
 * Created by PhpStorm.
 * User: v.zaica
 * Date: 8/1/2018
 * Time: 2:06 PM
 */

use Metro2\Writer;
use Metro2\Segment;
use Metro2\Field\Chars;
use Metro2\Field\Checkbox;
use Metro2\Field\Integer;
use PHPUnit\Framework\TestCase;

class WriterTest extends TestCase {
    private $example_1 = '1030320080000000PRCPMD3X              PRCPMD3XMD20501005451140000023I20010120070        20000    60 M386      0        711BBBBBBBBBBBBBBBBBBBBBBBB  0 19496    500               0401200802012008                EUR               PAPADOPOLESCU            IONUTZ              2680912703509        X1122434111251990241548309 201MDSTR ELIBERARII NR 2             Bl. IV45 Sc. A Et. 1 Ap. 4      CONSTANTA           091900     YO                                        VOICULESCU          STR TESTATRIII NR 2             Bl. IV45 Sc. A Et. 1 Ap. 4      BUCURESTI           031430                                                                                                                                                                                                                            ';

    private $example_header = 'HEADER  XXXTESTXXX                         08302018083020180830201808302018TEST REPORTER NAME                      TEST REPORTER ADDRESS                                                                           79123123  TEST SOFTWARE VENDOR                    1    ROro                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          ';

    private $example_trailer = 'TRAILER2                          0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0        0                                                                                                                                                                                                                                                                                                                                                                                                             ';

    public function testInstance() {
        $writer = new Writer();

        $this->assertEquals(true, $writer instanceof Writer);
    }

    public function testBaseSegment() {
        $base_segment = new Segment\Base();

        $base_segment->addField(new Checkbox\ProcessingIndicator());
        $base_segment->addField(new Chars\Timestamp(strtotime('2008-03-03 00:00:00')));
        $base_segment->addField(new Checkbox\CorrectionIndicator());
        $base_segment->addField(new Chars\IdentificationNumber('PRCPMD3X'));
        $base_segment->addField(new Chars\AccountNumber('PRCPMD3XMD20501005451140000023'));
        $base_segment->addField(new Chars\PortofolioType());
        $base_segment->addField(new Chars\AccountType('20'));
        $base_segment->addField(new Chars\DateOpened('2007-01-01'));
        $base_segment->addField(new Integer\CreditLimit(0));
        $base_segment->addField(new Integer\LoanAmount(20000));
        $base_segment->addField(new Chars\TermsDuration(60));
        $base_segment->addField(new Chars\TermsFrequency());
        $base_segment->addField(new Integer\ScheduledMonthlyPayment(386));
        $base_segment->addField(new Integer\ActualPaymentAmount(0));
        $base_segment->addField(new Chars\AccountStatus(Chars\AccountStatus::SUPERVISED));
        $base_segment->addField(new Chars\PaymentRating(Chars\PaymentRating::SUPERVISED));
        $base_segment->addField(new Chars\PaymentHistoryProfile());
        $base_segment->addField(new Chars\EmptyComment());
        $base_segment->addField(new Chars\InsuranceCode(Chars\InsuranceCode::NO_PLEDGE));
        $base_segment->addField(new Chars\ComplianceConditionCode());
        $base_segment->addField(new Integer\CurrentBalance(19496));
        $base_segment->addField(new Integer\AmountPastDue(500));
        $base_segment->addField(new Integer\OriginalChargeOffAmount(''));
        $base_segment->addField(new Chars\BillingDate('2008-04-01'));
        $base_segment->addField(new Chars\ComplianceDate('2008-02-01'));
        $base_segment->addField(new Chars\DateClosed(''));
        $base_segment->addField(new Chars\LastPaymentDate(''));
        $base_segment->addField(new Chars\CurrencyCode(Chars\CurrencyCode::EUR));
        $base_segment->addField(new Chars\ConsumerTransactionType());
        $base_segment->addField(new Chars\Surname('PAPADOPOLESCU'));
        $base_segment->addField(new Chars\Firstname('IONUTZ'));
        $base_segment->addField(new Chars\IdentificationCode('2680912703509'));
        $base_segment->addField(new Chars\GenerationCode());
        $base_segment->addField(new Chars\IdCardNumber('X11224341'));
        $base_segment->addField(new Chars\BirthDate('1990-11-25'));
        $base_segment->addField(new Chars\Phone('241548309'));
        $base_segment->addField(new Chars\ECOACode(Chars\ECOACode::HOLDER_SHARED));
        $base_segment->addField(new Chars\ConsumerInformationIndicator());
        $base_segment->addField(new Chars\CountryCode(Chars\CountryCode::MD));
        $base_segment->addField(new Chars\ResidenceAddress1('STR ELIBERARII NR 2'));
        $base_segment->addField(new Chars\ResidenceAddress2('Bl. IV45 Sc. A Et. 1 Ap. 4'));
        $base_segment->addField(new Chars\ResidenceCity('CONSTANTA'));
        $base_segment->addField(new Chars\ResidenceCounty(Chars\ResidenceCounty::TEST));
        $base_segment->addField(new Chars\ZipCode('1900'));
        $base_segment->addField(new Chars\ResidenceAddressIndicator(Chars\ResidenceAddressIndicator::ADDRESS_HOLDER));
        $base_segment->addField(new Chars\ResidenceCode(Chars\ResidenceCode::PERSONAL));
        $base_segment->addField(new Chars\Fax());
        $base_segment->addField(new Chars\Email());
        $base_segment->addField(new Chars\FatherName('VOICULESCU'));
        $base_segment->addField(new Chars\HomeAddress1('STR TESTATRIII NR 2'));
        $base_segment->addField(new Chars\HomeAddress2('Bl. IV45 Sc. A Et. 1 Ap. 4'));
        $base_segment->addField(new Chars\HomeCity('BUCURESTI'));
        $base_segment->addField(new Chars\HomeCounty(Chars\HomeCounty::BALTI));
        $base_segment->addField(new Chars\HomeZipCode('1430'));
        $base_segment->addField(new Chars\CompanyName(''));
        $base_segment->addField(new Chars\CompanyAbbreviation(''));
        $base_segment->addField(new Chars\CompanyInformation(''));

        $data = $base_segment->getFieldsData();

        $this->assertEquals(substr($this->example_1, 0, strlen($data)), $data);
    }

    public function testHeader() {
        $header = new Segment\Header();

        $header->addField(new Chars\ActivityDate('2018-08-30'));
        $header->addField(new Chars\CreatedDate('2018-08-30'));

        $this->assertEquals($this->example_header, $header->getFieldsData());
    }

    public function testTrailer() {
        $trailer = new Segment\Trailer();

        $trailer->set('base_segments', 2);

        //$this->assertEquals(strlen($this->example_trailer), strlen($trailer->getFieldsData()));
        $this->assertEquals($this->example_trailer, $trailer->getFieldsData());
    }
}
