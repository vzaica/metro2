<?php
namespace Metro2\Field\Integer;

use Metro2\Field\Integer;

class CurrentBalance extends Integer {
    const TYPE = parent::FIELD_CURRENT_BALANCE;

    public function __construct($value = 0) {
        parent::__construct();

        $this->setLength(9);
        $this->set($value);
    }
}