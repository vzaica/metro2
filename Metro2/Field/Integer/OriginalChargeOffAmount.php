<?php
namespace Metro2\Field\Integer;

use Metro2\Field\Integer;

class OriginalChargeOffAmount extends Integer {
    const TYPE = parent::FIELD_ORIGINAL_CHARGE_OFF_AMOUNT;

    public function __construct($value = 0) {
        parent::__construct();

        $this->setLength(9);
        $this->set($value);
    }
}