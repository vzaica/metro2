<?php
namespace Metro2\Field\Integer;

use Metro2\Field\Integer;

class CreditLimit extends Integer {
    const TYPE = parent::FIELD_CREDIT_LIMIT;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(9);
        $this->set($value);
    }
}