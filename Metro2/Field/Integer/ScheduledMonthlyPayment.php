<?php
namespace Metro2\Field\Integer;

use Metro2\Field\Integer;

class ScheduledMonthlyPayment extends Integer {
    const TYPE = parent::FIELD_SCHEDULED_MONTHLY_PAYMENT;

    public function __construct($value = 0) {
        parent::__construct();

        $this->setLength(9);
        $this->set($value);
    }
}