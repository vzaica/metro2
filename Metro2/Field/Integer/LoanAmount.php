<?php
namespace Metro2\Field\Integer;

use Metro2\Field\Integer;

class LoanAmount extends Integer {
    const TYPE = parent::FIELD_LOAN_AMOUNT;

    public function __construct($value = 0) {
        parent::__construct();

        $this->setLength(9);
        $this->set($value);
    }
}