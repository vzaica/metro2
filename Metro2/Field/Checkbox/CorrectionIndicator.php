<?php
namespace Metro2\Field\Checkbox;

use Metro2\Field\Checkbox;

class CorrectionIndicator extends Checkbox {
    const TYPE = parent::FIELD_CORRECTION_INDICATOR;

    public function __construct($value = 0) {
        parent::__construct();

        $this->set($value);
    }
}