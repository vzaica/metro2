<?php
namespace Metro2\Field\Checkbox;

use Metro2\Field\Checkbox;

class ProcessingIndicator extends Checkbox {
    const TYPE = parent::FIELD_PROCESSING_INDICATOR;

    public function __construct($value = 1) {
        parent::__construct();

        $this->set($value);
    }
}