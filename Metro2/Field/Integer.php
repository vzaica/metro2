<?php
namespace Metro2\Field;

use Metro2\Field;

class Integer extends Field {
    public function __construct($default_value = 0) {
        parent::__construct();

        $this->setLength(9);
        $this->set($default_value);
    }

    public function set($value) {
        if (empty($value) && $value !== 0) {
            parent::set('');
        } else {
            parent::set(round($value));
        }
    }
}