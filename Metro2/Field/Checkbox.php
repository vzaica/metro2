<?php
namespace Metro2\Field;

use Metro2\Field;

class Checkbox extends Field {
    public function __construct() {
        parent::__construct();

        $this->setLength(1);
    }

    public function set($value) {
        parent::set($value != 0 ? 1 : 0);
    }
}