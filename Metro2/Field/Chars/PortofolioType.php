<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class PortofolioType extends Chars {
    const TYPE = parent::FIELD_PORTOFOLIO_TYPE;

    const LEGAL        = 'C';
    const INDIVIDUAL   = 'I';
    const ENTREPRENEUR = 'M';
    const LEASING      = 'O';
    const CREDIT_CARD  = 'R';

    public function __construct($value = self::INDIVIDUAL) {
        parent::__construct();

        $this->setLength(1);
        $this->set($value);
    }
}