<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class ECOACode extends Chars {
    const TYPE = parent::FIELD_ECOA_CODE;

    const INDIVIDUAL                = '1';
    const HOLDER_SHARED             = '2';
    const HOLDER_GUARANTOR          = '7';
    const HOLDER_ASSOCIATION_ENDED  = 'T';
    const LIQUIDATED_SUBJECT_MATTER = 'X';
    const HOLDER_REMOVED            = 'Z';

    public function __construct($value = self::INDIVIDUAL) {
        parent::__construct();

        $this->setLength(1);
        $this->set($value);
    }
}