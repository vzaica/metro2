<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class ZipCode extends Chars {
    const TYPE = parent::FIELD_ZIP_CODE;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(9);
        $this->set($value);
    }
}