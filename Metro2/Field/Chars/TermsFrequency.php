<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class TermsFrequency extends Chars {
    const TYPE = parent::FIELD_TERMS_FREQUENCY;

    const WEEKLY      = 'W';
    const SEMIWEEKLY  = 'B';
    const SEMIMONTHLY = 'E';
    const MONTHLY     = 'M';
    const BIMONTHLY   = 'L';
    const QUARTERLY   = 'Q';
    const TRIANUAL    = 'T';
    const SEMESTRIAL  = 'S';
    const YEARLY      = 'Y';
    const RESCHEDULED = 'D';
    const SINGLE      = 'P';
    const UNDEFINED   = 'N';

    public function __construct($value = self::MONTHLY) {
        parent::__construct();

        $this->setLength(1);
        $this->set($value);
    }
}