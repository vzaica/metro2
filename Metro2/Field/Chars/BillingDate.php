<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class BillingDate extends Chars {
    const TYPE = parent::FIELD_BILLING_DATE;

    public function __construct($date = '') {
        parent::__construct();

        $this->setLength(8);
        $this->setDate($date);
    }

    public function setDate($date) {
        if (empty($date)) {
            parent::set('');
        } else {
            parent::set(date('mdY', strtotime($date)));
        }
    }

    public function getDate() {
        $string = parent::get();

        return strtotime(
            substr($string, 4, 4) . '-' .
            substr($string, 0, 2) . '-' .
            substr($string, 2, 2)
        );
    }
}