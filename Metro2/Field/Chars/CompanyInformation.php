<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class CompanyInformation extends Chars {
    const TYPE = parent::FIELD_COMPANY_INFORMATION;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(150);
        $this->set($value);
    }
}