<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class InvoisProgramIdentifier extends Chars {
    const TYPE = parent::FIELD_INVOIS_PROGRAM_IDENTIFIER;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(10);
        $this->set($value);
    }
}