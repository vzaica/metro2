<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class EmptyComment extends Chars {
    const TYPE = parent::FIELD_EMPTY_COMMENT;

    public function __construct($length = 2) {
        parent::__construct();

        $this->setLength($length);
        $this->set('');
    }
}