<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class Phone extends Chars {
    const TYPE = parent::FIELD_PHONE;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(10);
        $this->set($value);
    }
}