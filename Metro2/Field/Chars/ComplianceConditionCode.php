<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class ComplianceConditionCode extends Chars {
    const TYPE = parent::FIELD_COMPLIANCE_CONDITION_CODE;

    const NO_DATA    = ' ';
    const WEIRD      = '4';
    const COMPROMISE = '5';

    public function __construct($value = self::NO_DATA) {
        parent::__construct();

        $this->setLength(1);
        $this->set($value);
    }
}