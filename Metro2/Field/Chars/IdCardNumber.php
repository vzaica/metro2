<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class IdCardNumber extends Chars {
    const TYPE = parent::FIELD_ID_CARD_NUMBER;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(9);
        $this->set($value);
    }
}