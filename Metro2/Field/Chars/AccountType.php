<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class AccountType extends Chars {
    const TYPE = parent::FIELD_ACCOUNT_TYPE;

    const TYPE_CONSUMPTION       = '01';
    const TYPE_RETAIL            = '02';
    const TYPE_CAR               = '03';
    const TYPE_IMMOBILE          = '04';
    const TYPE_REPAIR            = '05';
    const TYPE_HOUSEHOLDS        = '06';
    const TYPE_BUSINESS          = '48';
    const TYPE_INVESTMENT        = '21';
    const TYPE_OPERATING         = '22';
    const TYPE_MIXED             = '23';
    const TYPE_WARRANTY          = '24';
    const TYPE_FACTORING         = '25';
    const TYPE_FORFEITING        = '26';
    const TYPE_INVESTMENT_LEGAL  = '41';
    const TYPE_OPERATING_LEGAL   = '42';
    const TYPE_MIXED_LEGAL       = '43';
    const TYPE_WARRANTY_LEGAL    = '44';
    const TYPE_FACTORING_LEGAL   = '45';
    const TYPE_FORFEITING_LEGAL  = '46';
    const TYPE_LEASING_CAR       = '71';
    const TYPE_LEASING_EQUIPMENT = '72';
    const TYPE_LEASING_IMMOBILE  = '73';
    const TYPE_CARD_OVERDRAFT    = '81';
    const TYPE_CARD_REGULAR      = '82';
    const TYPE_CARD_REVOLVING    = '83';
    const TYPE_CARD_OTHER        = '84';

    public function __construct($value = self::TYPE_CONSUMPTION) {
        parent::__construct();

        $this->setLength(2);
        $this->set($value);
    }
}