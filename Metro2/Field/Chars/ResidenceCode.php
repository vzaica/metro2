<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class ResidenceCode extends Chars {
    const TYPE = parent::FIELD_RESIDENCE_CODE;

    const PERSONAL = 'O';
    const RENTED   = 'R';
    const NONE     = ' ';

    public function __construct($value = self::NONE) {
        parent::__construct();

        $this->setLength(1);
        $this->set($value);
    }
}