<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class ConsumerTransactionType extends Chars {
    const TYPE = parent::FIELD_CONSUMER_TRANSACTION_TYPE;

    const NONE                = ' ';
    const NEW_CLIENT          = '1';
    const CHANGE_NAME         = '2';
    const CHANGE_ADDRESS      = '3';
    const CHANGE_NAME_ADDRESS = '6';

    public function __construct($value = self::NONE) {
        parent::__construct();

        $this->setLength(1);
        $this->set($value);
    }
}