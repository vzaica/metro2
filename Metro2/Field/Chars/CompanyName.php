<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class CompanyName extends Chars {
    const TYPE = parent::FIELD_COMPANY_NAME;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(45);
        $this->set($value);
    }
}