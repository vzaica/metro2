<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class CurrencyCode extends Chars {
    const TYPE = parent::FIELD_CURRENCY_CODE;

    const MDL = 'MDL';
    const EUR = 'EUR';

    public function __construct($value = self::MDL) {
        parent::__construct();

        $this->setLength(17);
        $this->set($value);
    }
}