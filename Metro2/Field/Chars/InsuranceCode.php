<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class InsuranceCode extends Chars {
    const TYPE = parent::FIELD_INSURANCE_CODE;

    const NO_PLEDGE                = '0';
    const IMMOBILE                 = '1';
    const PERSONAL                 = '2';
    const AUTOMOBILE               = '3';
    const EQUIPMENT                = '4';
    const BANK_DEPOSITS            = '5';
    const INVESTMENT_PAPERS        = '6';
    const AGRICULTURAL             = '7';
    const OTHERS                   = '8';
    const GOVERMENT                = '9';
    const STATE_PAPERS             = 'A';
    const INTERNATIONAL_GUARANTEES = 'B';
    const MOBILE_GOODS             = 'c';

    public function __construct($value = self::NO_PLEDGE) {
        parent::__construct();

        $this->setLength(1);
        $this->set($value);
    }
}