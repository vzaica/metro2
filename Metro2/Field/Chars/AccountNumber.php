<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class AccountNumber extends Chars {
    const TYPE = parent::FIELD_ACCOUNT_NUMBER;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(30);
        $this->set($value);
    }
}