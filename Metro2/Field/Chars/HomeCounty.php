<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class HomeCounty extends Chars {
    const TYPE = parent::FIELD_HOME_COUNTY;

    const CHISINAU = '01';
    const BALTI    = '03';

    public function __construct($value = self::CHISINAU) {
        parent::__construct();

        $this->setLength(2);
        $this->set($value);
    }
}