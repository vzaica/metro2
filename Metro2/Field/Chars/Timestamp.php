<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class Timestamp extends Chars {
    const TYPE = parent::FIELD_TIMESTAMP;

    public function __construct($timestamp = 0) {
        parent::__construct();

        $this->setLength(14);
        $this->setTimestamp($timestamp);
    }

    public function setTimestamp($timestamp) {
        parent::set(date('mdYHis', $timestamp));
    }

    public function getTimestamp() {
        $string = parent::get();

        return strtotime(
            substr($string, 4, 4) . '-' .
            substr($string, 0, 2) . '-' .
            substr($string, 2, 2) . '-' .
            substr($string, 8, 2) . '-' .
            substr($string, 10, 2) . '-' .
            substr($string, 12, 2)
        );
    }
}