<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class GenerationCode extends Chars {
    const TYPE = parent::FIELD_GENERATION_CODE;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(1);
        $this->set($value);
    }
}