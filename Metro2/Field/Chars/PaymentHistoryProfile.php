<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class PaymentHistoryProfile extends Chars {
    const TYPE = parent::FIELD_PAYMENT_HISTORY_PROFILE;

    const STANDARD           = '0';
    const SUPERVISED         = '1'; //31 - 60 DAYS
    const SUPERVISED2        = '2'; //61 - 90 DAYS
    const SUBSTANDARD        = '3'; //91 - 120 DAYS
    const SUBSTANDARD2       = '4'; //121 - 150 DAYS
    const SUBSTANDARD3       = '5'; //151 - 180 DAYS
    const WEIRD              = '6'; //181 -
    const NO_DATA            = 'B';
    const NO_DATA_MONTH      = 'D';
    const COLLECTION         = 'G';
    const PAYMENT_IN_ADVANCE = 'H';
    const RENUNCIATION       = 'J';
    const REENTRY            = 'K';
    const COMPROMIS          = 'L';
    const LIPSA              = 'N';

    private $values = [];

    public function __construct($values = []) {
        parent::__construct();

        $this->setLength(24);
        $this->setValues($values);
    }

    public function setValues($values = []) {
        $this->values = $values;

        $value = '';
        foreach($this->values as $single_value) {
            $value .= $single_value;
        }

        for($i = strlen($value) + 1; $i <= 24; $i++) {
            $value .= self::NO_DATA;
        }

        $this->set($value);
    }
}