<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class Email extends Chars {
    const TYPE = parent::FIELD_EMAIL;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(30);
        $this->set($value);
    }
}