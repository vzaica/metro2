<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class ResidenceAddressIndicator extends Chars {
    const TYPE = parent::FIELD_RESIDENCE_ADDRESS_INDICATOR;

    const ADDRESS_HOLDER    = 'Y';
    const ADDRESS_EXECUTIVE = 'B';
    const ADDRESS_RESIDENCE = 'I';
    const NONE = ' ';

    public function __construct($value = self::NONE) {
        parent::__construct();

        $this->setLength(1);
        $this->set($value);
    }
}