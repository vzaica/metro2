<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class IdentificationCode extends Chars {
    const TYPE = parent::FIELD_IDENTIFICATION_CODE;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(20);
        $this->set($value);
    }
}