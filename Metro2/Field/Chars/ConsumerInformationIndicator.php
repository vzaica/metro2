<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class ConsumerInformationIndicator extends Chars {
    const TYPE = parent::FIELD_CONSUMER_INFORMATION_INDICATOR;

    const INDIVIDUAL              = '01';
    const INDIVIDUAL_ENTREPRENEUR = '02';
    const LEGAL                   = '03';

    public function __construct($value = self::INDIVIDUAL) {
        parent::__construct();

        $this->setLength(2);
        $this->set($value);
    }
}