<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class Fax extends Chars {
    const TYPE = parent::FIELD_FAX;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(10);
        $this->set($value);
    }
}