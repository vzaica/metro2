<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class CompanyAbbreviation extends Chars {
    const TYPE = parent::FIELD_COMPANY_ABBREVIATION;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(20);
        $this->set($value);
    }
}