<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class TermsDuration extends Chars {
    const TYPE = parent::FIELD_TERMS_DURATION;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(3);
        $this->set($value);
    }
}