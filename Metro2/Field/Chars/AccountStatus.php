<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class AccountStatus extends Chars {
    const TYPE = parent::FIELD_ACCOUNT_STATUS;

    const LIPSA                       = 'NA';
    const OPEN                        = '06';
    const ACTIVE                      = '11';
    const CLOSED                      = '13';
    const CLOSED_IN_ADVANCE           = '61';
    const CLOSED_COLLECTION           = '62';
    const CLOSED_REINSTATEMENT        = '63';
    const CLOSED_EXTRABILANCE         = '64';
    const CLOSED_FORCED               = '65';
    const CLOSED_SALE                 = '67';
    const SUPERVISED                  = '71'; //31 - 60 DAYS
    const SUPERVISED2                 = '78'; //61 - 90 DAYS
    const SUBSTANDARD                 = '80'; //91 - 120 DAYS
    const SUBSTANDARD2                = '82'; //121 - 150 DAYS
    const SUBSTANDARD3                = '83'; //151 - 180 DAYS
    const WEIRD                       = '84'; //181 -
    const COMPROMISE                  = '90';
    const COLLECTION                  = '93';
    const SALE_GUARANTEE              = '94';
    const RETURN_SUBJECT              = '95';
    const RETURN_SUBJECT_NEED_PAYMENT = '96';
    const CLOSED_UNPAID               = '97';
    const DELETE                      = 'DA';

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(2);
        $this->set($value);
    }
}