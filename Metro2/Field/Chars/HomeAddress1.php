<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class HomeAddress1 extends Chars {
    const TYPE = parent::FIELD_HOME_ADDRESS_1;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(32);
        $this->set($value);
    }
}