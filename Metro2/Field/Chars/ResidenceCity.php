<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class ResidenceCity extends Chars {
    const TYPE = parent::FIELD_RESIDENCE_CITY;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(20);
        $this->set($value);
    }
}