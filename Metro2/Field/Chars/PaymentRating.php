<?php

namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class PaymentRating extends Chars {
    const TYPE = parent::FIELD_PAYMENT_RATING;

    const STANDARD     = '0';
    const SUPERVISED   = '1'; //31 - 60 DAYS
    const SUPERVISED2  = '2'; //61 - 90 DAYS
    const SUBSTANDARD  = '3'; //91 - 120 DAYS
    const SUBSTANDARD2 = '4'; //121 - 150 DAYS
    const SUBSTANDARD3 = '5'; //151 - 180 DAYS
    const WEIRD        = '6'; //181 -
    const COLLECTION   = 'G';
    const COMPROMISE   = 'L';
    const LIPSA        = 'N';

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(1);
        $this->set($value);
    }
}