<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class Firstname extends Chars {
    const TYPE = parent::FIELD_FIRST_NAME;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(20);
        $this->set($value);
    }
}