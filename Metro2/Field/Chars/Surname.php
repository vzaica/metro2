<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class Surname extends Chars {
    const TYPE = parent::FIELD_SURNAME;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(25);
        $this->set($value);
    }
}