<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class FatherName extends Chars {
    const TYPE = parent::FIELD_FATHER_NAME;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(20);
        $this->set($value);
    }
}