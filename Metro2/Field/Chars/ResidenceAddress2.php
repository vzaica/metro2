<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class ResidenceAddress2 extends Chars {
    const TYPE = parent::FIELD_RESIDENCE_ADDRESS_2;

    public function __construct($value = '') {
        parent::__construct();

        $this->setLength(32);
        $this->set($value);
    }
}