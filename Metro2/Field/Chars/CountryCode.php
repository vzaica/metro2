<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class CountryCode extends Chars {
    const TYPE = parent::FIELD_COUNTRY_CODE;

    const MD = 'MD';

    public function __construct($value = self::MD) {
        parent::__construct();

        $this->setLength(2);
        $this->set($value);
    }
}