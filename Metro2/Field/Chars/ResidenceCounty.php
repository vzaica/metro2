<?php
namespace Metro2\Field\Chars;

use Metro2\Field\Chars;

class ResidenceCounty extends Chars {
    const TYPE = parent::FIELD_RESIDENCE_COUNTY;

    const CHISINAU = '01';
    const TEST = '09';

    public function __construct($value = self::CHISINAU) {
        parent::__construct();

        $this->setLength(2);
        $this->set($value);
    }
}