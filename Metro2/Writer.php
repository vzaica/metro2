<?php
namespace Metro2;

class Writer {
    private $header;
    private $base_segments = [];

    public function __construct() {

    }

    public function setHeader($header) {
        $this->header = $header;
    }

    public function addBaseSegment($base_segment) {
        $this->base_segments[] = $base_segment;
    }

    public function export() {
        $data = $this->header->export()."\n";

        $status_codes = [];
        $ecoa_z = 0;
        $n1_segments = 0;
        $k1_segments = 0;
        $k2_segments = 0;
        $k3_segments = 0;
        $k4_segments = 0;
        $l1_segments = 0;
        $all_idnp_idno = 0;
        $base_idnp_idno = 0;
        $all_birth_date = 0;
        $base_birth_date = 0;
        $all_phone = 0;

        foreach($this->base_segments as $index => $segment) {
            $data .= $segment->export();

            $data .= "\n";

            $status_code = $segment->getField(Field::FIELD_ACCOUNT_STATUS)->get();

            if (!isset($status_codes[$status_code])) {
                $status_codes[$status_code] = 0;
            }

            $status_codes[$status_code]++;

            if ($segment->getField(Field::FIELD_ECOA_CODE)->get() == 'Z') {
                $ecoa_z++;
            }

            if (!empty($segment->getField(Field::FIELD_IDENTIFICATION_CODE)->get())) {
                $all_idnp_idno++;
                $base_idnp_idno++;
            }

            if ($segment->getField(Field::FIELD_BIRTH_DATE)->get() != '01011970') {
                $all_birth_date++;
                $base_birth_date++;
            }

            if ($segment->getField(Field::FIELD_PHONE)->get() != '') {
                $all_phone++;
            }
        }

        $trailer = new Segment\Trailer();

        $trailer->set('base_segments', count($this->base_segments));
        $trailer->set('block_count', count($this->base_segments));

        foreach($status_codes as $code => $count) {
            $trailer->set('status_code_' . strtolower($code), $count);
        }

        $trailer->set('ecoa_code_z', $ecoa_z);
        $trailer->set('n1_segments', $n1_segments);
        $trailer->set('k1_segments', $k1_segments);
        $trailer->set('k2_segments', $k2_segments);
        $trailer->set('k3_segments', $k3_segments);
        $trailer->set('k4_segments', $k4_segments);
        $trailer->set('l1_segments', $l1_segments);
        $trailer->set('all_idnp_idno', $all_idnp_idno);
        $trailer->set('base_idnp_idno', $base_idnp_idno);
        $trailer->set('all_birth_date', $all_birth_date);
        $trailer->set('base_birth_date', $base_birth_date);
        $trailer->set('all_phone', $all_phone);

        $data .= $trailer->export();

        return $data;
    }

    public function save($path) {
        file_put_contents($path, $this->export());
    }
}