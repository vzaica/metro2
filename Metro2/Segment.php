<?php

namespace Metro2;

class Segment {
    private $fields = array();
    private $fields_order = array();

    public function getField($type) {
        return $this->fields[$type];
    }

    public function addField($field) {
        $this->fields[$field::TYPE] = $field;
    }

    public function setFieldsOrder($order) {
        $this->fields_order = $order;
    }

    public function getFieldsData() {
        $data = '';
        foreach($this->fields_order as $field) {
            if (isset($this->fields[$field])) {
                $data .= $this->fields[$field]->export();
            } else {
                $data .= $field;
            }
        }

        return $data;
    }

    public function getDataWithRDW($data) {
        $rdw = strlen($data) + 4;
        $rdw = str_repeat('0', 4 - strlen($rdw)).$rdw;

        $data = $rdw.$data;

        return $data;
    }

    public function getDataWithBDW($data) {
        $bdw = strlen($data) + 4;
        $bdw = str_repeat('0', 4 - strlen($bdw)).$bdw;

        $data = $bdw.$data;

        return $data;
    }

    public function export() {
        return $this->getDataWithBDW($this->getDataWithRDW($this->getFieldsData()));
    }
}