<?php
namespace Metro2\Exception;

use Metro2\Exception;

class Field extends Exception {
    private $field;

    public function setField($field) {
        $this->field = $field;

        $this->message .= ', Field: ' . $field::TYPE;
    }
}