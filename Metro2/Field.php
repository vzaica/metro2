<?php

namespace Metro2;

use Metro2\Exception;

class Field {
    const FIELD_PROCESSING_INDICATOR           = "processing_indicator";
    const FIELD_TIMESTAMP                      = "timestamp";
    const FIELD_CORRECTION_INDICATOR           = "correction_indicator";
    const FIELD_IDENTIFICATION_NUMBER          = "identification_number";
    const FIELD_ACCOUNT_NUMBER                 = "account_number";
    const FIELD_PORTOFOLIO_TYPE                = "portofolio_type";
    const FIELD_ACCOUNT_TYPE                   = "account_type";
    const FIELD_DATE_OPENED                    = "date_opened";
    const FIELD_CREDIT_LIMIT                   = "credit_limit";
    const FIELD_LOAN_AMOUNT                    = "loan_amount";
    const FIELD_TERMS_DURATION                 = "terms_duration";
    const FIELD_TERMS_FREQUENCY                = "terms_frequency";
    const FIELD_SCHEDULED_MONTHLY_PAYMENT      = "scheduled_monthly_payment";
    const FIELD_ACTUAL_PAYMENT_AMOUNT          = "actual_payment_amount";
    const FIELD_ACCOUNT_STATUS                 = "account_status";
    const FIELD_PAYMENT_RATING                 = "payment_rating";
    const FIELD_PAYMENT_HISTORY_PROFILE        = "payment_history_profile";
    const FIELD_EMPTY_COMMENT                  = "empty_comment";
    const FIELD_INSURANCE_CODE                 = "insurance_code";
    const FIELD_COMPLIANCE_CONDITION_CODE      = "compliance_condition_code";
    const FIELD_CURRENT_BALANCE                = "current_balance";
    const FIELD_AMOUNT_PAST_DUE                = "amount_past_due";
    const FIELD_ORIGINAL_CHARGE_OFF_AMOUNT     = "original_charge_off_amount";
    const FIELD_BILLING_DATE                   = "billing_date";
    const FIELD_COMPLIANCE_DATE                = "compliance_date";
    const FIELD_DATE_CLOSED                    = "date_closed";
    const FIELD_LAST_PAYMENT_DATE              = "last_payment_date";
    const FIELD_CURRENCY_CODE                  = "currency_code";
    const FIELD_CONSUMER_TRANSACTION_TYPE      = "consumer_transaction_type";
    const FIELD_SURNAME                        = "surname";
    const FIELD_FIRST_NAME                     = "firstname";
    const FIELD_IDENTIFICATION_CODE            = "identification_code";
    const FIELD_GENERATION_CODE                = "generation_code";
    const FIELD_ID_CARD_NUMBER                 = "id_card_number";
    const FIELD_BIRTH_DATE                     = "birth_date";
    const FIELD_PHONE                          = "phone";
    const FIELD_ECOA_CODE                      = "ecoa_code";
    const FIELD_CONSUMER_INFORMATION_INDICATOR = "consumer_information_indicator";
    const FIELD_COUNTRY_CODE                   = "country_code";
    const FIELD_RESIDENCE_ADDRESS_1            = "residence_address_1";
    const FIELD_RESIDENCE_ADDRESS_2            = "residence_address_2";
    const FIELD_RESIDENCE_CITY                 = "residence_city";
    const FIELD_RESIDENCE_COUNTY               = "residence_county";
    const FIELD_ZIP_CODE                       = "zip_code";
    const FIELD_RESIDENCE_ADDRESS_INDICATOR    = "residence_address_indicator";
    const FIELD_RESIDENCE_CODE                 = "residence_code";
    const FIELD_FAX                            = "fax";
    const FIELD_EMAIL                          = "email";
    const FIELD_FATHER_NAME                    = "father_name";
    const FIELD_HOME_ADDRESS_1                 = "home_address_1";
    const FIELD_HOME_ADDRESS_2                 = "home_address_2";
    const FIELD_HOME_CITY                      = "home_city";
    const FIELD_HOME_COUNTY                    = "home_country";
    const FIELD_HOME_ZIP_CODE                  = "home_zip_code";
    const FIELD_COMPANY_NAME                   = "company_name";
    const FIELD_COMPANY_ABBREVIATION           = "company_abbreviation";
    const FIELD_COMPANY_INFORMATION            = "company_information";

    //Header
    const FIELD_INVOIS_PROGRAM_IDENTIFIER = "invois_program_identifier";
    const FIELD_ACTIVITY_DATE             = "activity_date";
    const FIELD_CREATED_DATE              = "created_date";
    const FIELD_PROGRAM_DATE              = "program_date";
    const FIELD_PROGRAM_REVISION_DATE     = "program_revision_date";

    private $value;
    private $length;

    public function __construct() {

    }

    public function set($value) {
        if (strlen($value) > $this->getLength()) {
            $exception = new Exception\Field('Field length must be less or equal to ' . $this->getLength());
            $exception->setField($this);

            throw $exception;
        }

        $this->value = $value;
    }

    public function get() {
        return $this->value;
    }

    public function getLength() {
        return $this->length;
    }

    public function setLength($length) {
        $this->length = $length;
    }

    public function export() {
        return $this->get() . str_repeat(' ', $this->getLength() - strlen($this->get()));
    }
}