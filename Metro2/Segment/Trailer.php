<?php

namespace Metro2\Segment;

use Metro2;
use Metro2\Segment;
use Metro2\Field;

class Trailer extends Segment {
    private $fields = array();

    public function __construct() {
        $this->fields = array(
            'base_segments'   => 0,
            'j1_segments'     => 0,
            'j2_segments'     => 0,
            'block_count'     => 0,
            'status_code_da'  => 0,
            'status_code_05'  => 0,
            'status_code_11'  => 0,
            'status_code_13'  => 0,
            'status_code_61'  => 0,
            'status_code_62'  => 0,
            'status_code_63'  => 0,
            'status_code_64'  => 0,
            'status_code_65'  => 0,
            'status_code_71'  => 0,
            'status_code_78'  => 0,
            'status_code_80'  => 0,
            'status_code_82'  => 0,
            'status_code_83'  => 0,
            'status_code_84'  => 0,
            'status_code_88'  => 0,
            'status_code_89'  => 0,
            'status_code_93'  => 0,
            'status_code_94'  => 0,
            'status_code_95'  => 0,
            'status_code_96'  => 0,
            'status_code_97'  => 0,
            'ecoa_code_z'     => 0,
            'n1_segments'     => 0,
            'k1_segments'     => 0,
            'k2_segments'     => 0,
            'k3_segments'     => 0,
            'k4_segments'     => 0,
            'l1_segments'     => 0,
            'all_idnp_idno'   => 0,
            'base_idnp_idno'  => 0,
            'j1_idnp_idno'    => 0,
            'j2_idnp_idno'    => 0,
            'all_birth_date'  => 0,
            'base_birth_date' => 0,
            'j1_birth_date'   => 0,
            'j2_birth_date'   => 0,
            'all_phone'       => 0
        );
    }

    public function getFieldsData() {
        $fields = ['TRAILER'];

        $reserved = new Field();
        $reserved->setLength(18);
        $reserved->set('');

        $reserved_last = new Field();
        $reserved_last->setLength(389);
        $reserved_last->set('');

        foreach($this->fields as $key => $field) {
            $fields[] = (new Field\Integer($field))->export();

            if ($key == 'base_segments') {
                $fields[] = $reserved->export();
            }
        }

        $fields[] = $reserved_last->export();

        $this->setFieldsOrder($fields);

        return parent::getFieldsData();
    }

    public function set($key, $value) {
        if (isset($this->fields[$key])) {
            $this->fields[$key] = $value;
        }
    }
}