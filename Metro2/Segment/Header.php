<?php
namespace Metro2\Segment;

use Metro2;
use Metro2\Segment;
use Metro2\Field;

class Header extends Segment {
    public function __construct() {
        $cycle_number = new Field();
        $cycle_number->setLength(2);
        $cycle_number->set('');

        $equifax = new Field();
        $equifax->setLength(10);
        $equifax->set('');

        $experian = new Field();
        $experian->setLength(5);
        $experian->set('');

        $transunion = new Field();
        $transunion->setLength(10);
        $transunion->set('');

        $reserved = new Field();
        $reserved->setLength(522);
        $reserved->set('');

        $this->setFieldsOrder([
            'HEADER',
            $cycle_number->export(),
            Metro2::getInvoisProgramIdentifier()->export(),
            $equifax->export(),
            $experian->export(),
            $transunion->export(),
            Field::FIELD_ACTIVITY_DATE,
            Field::FIELD_CREATED_DATE,
            Metro2::getProgramDate()->export(),
            Metro2::getProgramRevisionDate()->export(),
            Metro2::getReporterName()->export(),
            Metro2::getReporterAddress()->export(),
            Metro2::getReporterPhone()->export(),
            Metro2::getSoftwareVendor()->export(),
            Metro2::getSoftwareVersion()->export(),
            Metro2::getLanguage()->export(),
            $reserved->export()
        ]);
    }
}