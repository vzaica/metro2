<?php
namespace Metro2\Segment;

use Metro2\Segment;
use Metro2\Field;

class Base extends Segment {
    public function __construct() {
        $cycle_identifier = new Field();
        $cycle_identifier->setLength(2);
        $cycle_identifier->set('');

        $this->setFieldsOrder([
            Field::FIELD_PROCESSING_INDICATOR,
            Field::FIELD_TIMESTAMP,
            Field::FIELD_CORRECTION_INDICATOR,
            Field::FIELD_IDENTIFICATION_NUMBER,
            $cycle_identifier->export(),
            Field::FIELD_ACCOUNT_NUMBER,
            Field::FIELD_PORTOFOLIO_TYPE,
            Field::FIELD_ACCOUNT_TYPE,
            Field::FIELD_DATE_OPENED,
            Field::FIELD_CREDIT_LIMIT,
            Field::FIELD_LOAN_AMOUNT,
            Field::FIELD_TERMS_DURATION,
            Field::FIELD_TERMS_FREQUENCY,
            Field::FIELD_SCHEDULED_MONTHLY_PAYMENT,
            Field::FIELD_ACTUAL_PAYMENT_AMOUNT,
            Field::FIELD_ACCOUNT_STATUS,
            Field::FIELD_PAYMENT_RATING,
            Field::FIELD_PAYMENT_HISTORY_PROFILE,
            Field::FIELD_EMPTY_COMMENT,
            Field::FIELD_INSURANCE_CODE,
            Field::FIELD_COMPLIANCE_CONDITION_CODE,
            Field::FIELD_CURRENT_BALANCE,
            Field::FIELD_AMOUNT_PAST_DUE,
            Field::FIELD_ORIGINAL_CHARGE_OFF_AMOUNT,
            Field::FIELD_BILLING_DATE,
            Field::FIELD_COMPLIANCE_DATE,
            Field::FIELD_DATE_CLOSED,
            Field::FIELD_LAST_PAYMENT_DATE,
            Field::FIELD_CURRENCY_CODE,
            Field::FIELD_CONSUMER_TRANSACTION_TYPE,
            Field::FIELD_SURNAME,
            Field::FIELD_FIRST_NAME,
            Field::FIELD_IDENTIFICATION_CODE,
            Field::FIELD_GENERATION_CODE,
            Field::FIELD_ID_CARD_NUMBER,
            Field::FIELD_BIRTH_DATE,
            Field::FIELD_PHONE,
            Field::FIELD_ECOA_CODE,
            Field::FIELD_CONSUMER_INFORMATION_INDICATOR,
            Field::FIELD_COUNTRY_CODE,
            Field::FIELD_RESIDENCE_ADDRESS_1,
            Field::FIELD_RESIDENCE_ADDRESS_2,
            Field::FIELD_RESIDENCE_CITY,
            Field::FIELD_RESIDENCE_COUNTY,
            Field::FIELD_ZIP_CODE,
            Field::FIELD_RESIDENCE_ADDRESS_INDICATOR,
            Field::FIELD_RESIDENCE_CODE,
            Field::FIELD_FAX,
            Field::FIELD_EMAIL,
            Field::FIELD_FATHER_NAME,
            Field::FIELD_HOME_ADDRESS_1,
            Field::FIELD_HOME_ADDRESS_2,
            Field::FIELD_HOME_CITY,
            Field::FIELD_HOME_COUNTY,
            Field::FIELD_HOME_ZIP_CODE,
            Field::FIELD_COMPANY_NAME,
            Field::FIELD_COMPANY_ABBREVIATION,
            Field::FIELD_COMPANY_INFORMATION
        ]);
    }

    public function get() {
        $data = $this->getFieldsData();

        return $data;
    }
}