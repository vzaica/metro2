<?php
use Metro2\Field\Chars;

use Metro2\Field;

class Metro2 {
    public static function getInvoisProgramIdentifier() {
        $identifier = new Chars\InvoisProgramIdentifier('XXXTESTXXX');

        return $identifier;
    }

    public static function getProgramDate() {
        $date = new Chars\ProgramDate('2018-08-30');

        return $date;
    }

    public static function getProgramRevisionDate() {
        $date = new Chars\ProgramRevisionDate('2018-08-30');

        return $date;
    }

    public static function getReporterName() {
        $name = new Field();
        $name->setLength(40);
        $name->set('TEST REPORTER NAME');

        return $name;
    }

    public static function getReporterAddress() {
        $name = new Field();
        $name->setLength(96);
        $name->set('TEST REPORTER ADDRESS');

        return $name;
    }

    public static function getReporterPhone() {
        $name = new Field();
        $name->setLength(10);
        $name->set('79123123');

        return $name;
    }

    public static function getSoftwareVendor() {
        $name = new Field();
        $name->setLength(40);
        $name->set('TEST SOFTWARE VENDOR');

        return $name;
    }

    public static function getSoftwareVersion() {
        $name = new Field();
        $name->setLength(5);
        $name->set('1');

        return $name;
    }

    public static function getLanguage() {
        $name = new Field();
        $name->setLength(4);
        $name->set('ROro');

        return $name;
    }
}